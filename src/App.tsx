import React, {useEffect} from 'react';
import { WindowType } from './windowType';
import logo from './logo.svg';
import './App.css';

declare const window: WindowType;

function App() {
    const initEmbeddedMessaging = () =>{
      try {
          window.embeddedservice_bootstrap.settings.language = 'en_US'; // For example, enter 'en' or 'en-US'

          window.embeddedservice_bootstrap.init(
              '00D3N000000HG1U',
              'In_app_message_web',
              'https://vpcare--full.sandbox.my.site.com/ESWInappmessageweb1700741112297',
              {
                  scrt2URL: 'https://vpcare--full.sandbox.my.salesforce-scrt.com'
              }
          );
      } catch (err) {
          console.error('Error loading Embedded Messaging: ', err);
      }
      window.addEventListener("onEmbeddedMessagingReady", e => {
            
        window.embeddedservice_bootstrap.prechatAPI.setHiddenPrechatFields({"Order_Number" : "867-5309",
        "Subject" : "Test case"});

        window.embeddedservice_bootstrap.autoResponseAPI.setAutoResponseParameters({
            customer_name : "Test developer",
            agent_name: "ABC agent",
          });

        /*embeddedservice_bootstrap.prechatAPI.setVisiblePrechatFields({
          // List the pre-chat field names with the value and whether
          // it's editable in the pre-chat form.
          "_firstname": {
            "value": "Jane",
            "isEditableByEndUser": true
          },
          "_lastname": {
            "value": "Dave",
            "isEditableByEndUser": true
          },
          "_email": {
            "value": "Jane.Dave@gmail.com",
            "isEditableByEndUser": true
          }
        });*/
      });
      window.addEventListener("onEmbeddedMessagingReady", () => {
        console.log("Received the onEmbeddedMessagingReady event.");
      });
      window.addEventListener("onEmbeddedMessagingIdentityTokenExpired", () => {
        console.log("Received the onEmbeddedMessagingIdentityTokenExpired event.");
      });
  };
  useEffect(() => {
    var script = document.createElement("script")
    script.type = "text/javascript";
    script.onload = function(){
        initEmbeddedMessaging();
    };
    script.src = "https://vpcare--full.sandbox.my.site.com/ESWInappmessageweb1700741112297/assets/js/bootstrap.min.js";
    document.getElementsByTagName("body")[0].appendChild(script);
  },[])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
