/* eslint-disable @typescript-eslint/no-explicit-any */
export interface WindowType {
    webkitAudioContext: any;
    AudioContext: any;
    dispatchEvent(arg0: CustomEvent<unknown>): unknown;
    addEventListener(arg0: string, arg1: (e: any) => void): unknown;
    // eslint-disable-next-line camelcase
    embeddedservice_bootstrap: any;
    location: {
      hostname: string;
      pathname: string;
      href: string;
    };
    tracking: any;
    CareEmbeddedChatRollbar: any;
    vp: {
      auth: {
        cache: {
          isSignedIn: boolean;
          accessToken: string;
        };
      };
    };
    dataLayer: any;
    performance: any;
    helpPanelLoaded: any;
    CALoaded: any;
    isStudioReviewPage: any;
  }
  